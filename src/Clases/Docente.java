/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author angel
 */
public class Docente extends Empleado{
    private int nivel;
    private float horas;
    private float pagoHora;

    public Docente() {
        this.nivel = 0;
        this.horas = 0.0f;
        this.pagoHora = 0.0f;
    }

    public Docente(int nivel, float horas, float pagoHora) {
        this.nivel = nivel;
        this.horas = horas;
        this.pagoHora = pagoHora;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    @Override
    public float calcularTotal() {
        float total = 0.0f;
     switch(this.nivel){
         case 1:
             total = this.horas * this.pagoHora * 0.35f;
             break;
         case 2:
             total = this.horas * this.pagoHora * 0.4f;
             break;
         case 3:
             total = this.horas * this.pagoHora * 0.5f;
             break;
     }   
     return total;
    }

    @Override
    public float calcularImpuesto() {
        return this.horas * this.pagoHora * this.contrato.getImpuesto()/100;
    }
    
}
